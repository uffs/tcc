const FileSystem = require('fs')

const artigos = FileSystem.readFileSync('../artigos.json', 'utf-8')
const array_artigos = JSON.parse(artigos)

array_artigos.filter(artigo => !artigo.motivo_eliminacao)
  .forEach(({id, titulo, link}) => console.log(`\x1b[1m${id}\x1b[0m: ${titulo} | \x1b[34m${link}\x1b[0m`))