const statusArtigo = artigo => {
  let status = '';
  if (artigo.motivo_eliminacao) { status += `❌`}
  else if (!artigo.motivo_eliminacao && artigo.observacoes) { status += `✅`}
  else if (!artigo.motivo_eliminacao && !artigo.observacoes) { status += `❓`}

  if (artigo.referencial_teorico) { status += `[RF]`}
  if (artigo.artigo_relacionado) { status += `[AR]`}
  return status
}


const FileSystem = require('fs')


const artigos = FileSystem.readFileSync('../artigos.json', 'utf-8')
const array_artigos = JSON.parse(artigos)
array_artigos.forEach((artigo) => {
  if (artigo.motivo_eliminacao || !artigo.observacoes) { return; }
  let string = `${statusArtigo(artigo)} ${artigo.titulo}
${artigo.link}
Observações Igor: ${artigo.observacoes}
Quantidade de citações: ${artigo.quantidade_citacoes} - ${artigo.link_citacoes}
Observações Dênio:

`
  console.log(string)
})