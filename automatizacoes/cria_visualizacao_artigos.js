const statusArtigo = artigo => {
  let status = '';
  if (artigo.motivo_eliminacao) { status += `❌`}
  else if (!artigo.motivo_eliminacao && artigo.observacoes) { status += `✅`}
  else if (!artigo.motivo_eliminacao && !artigo.observacoes) { status += `❓`}

  if (artigo.referencial_teorico) { status += `[RF]`}
  if (artigo.artigo_relacionado) { status += `[AR]`}
  return status
}


const FileSystem = require('fs')


const artigos = FileSystem.readFileSync('../artigos.json', 'utf-8')
const array_artigos = JSON.parse(artigos)
let string_output = ''
string_output += `Legenda:  \n`
string_output += `❌ - Artigo eliminado  \n`
string_output += `✅ - Artigo "aceito"  \n`
string_output += `❓ - Artigo ainda não avaliado  \n`
string_output += `[RF] - Referencial Teórico  \n`
string_output += `[AR] - Artigo Relacionado  \n`
string_output += `\n`
string_output +=  `|ID|Status|Título|Link|Citações|Motivo de Eliminação|Observações|\n`
string_output += `|---|------|------|----|--------|--------------------|-----------|\n`
string_artigos = array_artigos.sort((artigo_a, artigo_b) => {
  if (artigo_a.motivo_eliminacao && artigo_b.motivo_eliminacao) {
    return artigo_a.id - artigo_b.id;
  }
  if (artigo_a.motivo_eliminacao) { return 1; }
  if (artigo_b.motivo_eliminacao) { return -1; }
  if (artigo_a.observacoes && artigo_b.observacoes) { return 0; }
  if (artigo_a.observacoes) { return -1; }
  if (artigo_b.observacoes) { return 1; }
  return artigo_a.id - artigo_b.id;
}).map(artigo => (
  `|${artigo.id}|${statusArtigo(artigo)}|${artigo.titulo}|[Link](${artigo.link})|[${artigo.quantidade_citacoes}](${artigo.link_citacoes})|${artigo.motivo_eliminacao}|${artigo.observacoes}|`
))
string_output += string_artigos.join("\n")

FileSystem.writeFileSync('../visualizacao/artigos.md', string_output)