/* Leitura dos artigos */
const FileSystem = require('fs')
const ATRIBUTOS_A_SEREM_ADICIONADOS = ['motivo_eliminacao']

const artigos = FileSystem.readFileSync('../artigos.json', 'utf-8')
const array_artigos = JSON.parse(artigos)

/* Adiciona os atributos */
const novos_artigos = array_artigos.map((artigo) => {
  ATRIBUTOS_A_SEREM_ADICIONADOS.forEach((atributo) => {
    if (artigo[atributo]) { return; }
    artigo[atributo] = ''
  })
  return artigo
})

/* Salva os artigos */
const saida_artigos = FileSystem.writeFileSync('../artigos.json', JSON.stringify(novos_artigos, null, 2))