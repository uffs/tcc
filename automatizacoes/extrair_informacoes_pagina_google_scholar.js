/** Script rodado nas páginas do google scholar para dar informações dos artigos buscados em formato de JSON. Para copiar as informações, clique com o direito no array que foi impresso no console e selecione "Copiar objeto" */

const STRING = `((("topic model" OR "topic modeling") AND ("court cases" OR "court documents" OR "law documents" OR "legal texts")) OR (("modelagem de tópicos") AND ("jurídicos" OR "judicial" OR "jurídico" OR "judiciais"))) AND ("lda" OR "latent dirichlet allocation")`
const extractInfo = (page) => {
  return Array.from(document.querySelectorAll('.gs_rt > a')).map((element) => {
    const artigo = {}
    const parent = element.closest('.gs_ri')
    artigo.titulo = element.innerHTML
    artigo.link = element.href
    artigo.pagina_google_scholar = page
    artigo.string_busca = STRING

    const cited_by_anchor = parent.querySelector('.gs_fl').children[2]
    if (cited_by_anchor.innerHTML.match(/Cited by/)) {
      artigo.quantidade_citacoes = cited_by_anchor.innerHTML.match(/Cited by (\d*)/)[1]
      artigo.link_citacoes = cited_by_anchor.href
    }
    return artigo
  })
};