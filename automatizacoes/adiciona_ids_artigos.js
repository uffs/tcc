

/* Leitura dos artigos */
const FileSystem = require('fs')

const artigos = FileSystem.readFileSync('../artigos.json', 'utf-8')
const array_artigos = JSON.parse(artigos)

/* Função para criar ID */
const proximo_id_valido = (array_ids) => {
  let id = 1;
  while (array_ids.find(artigo_id => id === artigo_id)) { id++; }
  return id;
}

/* Inicializa a lista de IDS que estão sendo utilizados */
const lista_ids = array_artigos.reduce((array, artigo) => (
  artigo.id ? [...array, artigo.id] : array
), [])

/* Adiciona ID aos artigos que não tem */
artigos_com_id = array_artigos.map((artigo) => {
  if (artigo.id) { return artigo; }

  const novo_id_artigo = proximo_id_valido(lista_ids);
  lista_ids.push(novo_id_artigo)
  return { id: novo_id_artigo, ...artigo }
})

/* Salvo os artigos */
const saida_artigos = FileSystem.writeFileSync('../artigos.json', JSON.stringify(artigos_com_id, null, 2))