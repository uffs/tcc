# Para criar o referencial teórico
1. Criei a string de busca: `((("topic model" OR "topic modeling") AND ("court cases" OR "court documents" OR "law documents" OR "legal texts")) OR (("modelagem de tópicos") AND ("jurídicos" OR "judicial" OR "jurídico" OR "judiciais"))) AND ("lda" OR "latent dirichlet allocation")`
1. Busquei no Google Scholar
1. Rodei, para cada página, o script [para buscar as informações no layout da página e devolver o nome e o link do artigo](./automatizacoes/extrair_informacoes_pagina_google_scholar.js)
1. 