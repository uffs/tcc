55 - Latent Dirichlett Allocation

# Question
- O que é um modelo Bayesiano?
- O que significa "three-level hierarchical Bayesian model"?
- O que é uma "approximate inference technique"?
- O que são métodos variacionais?
- O que é Bayes parameters estimation?
- O que é o modelo probabilístico LSI?
- O que é o modelo _unigram_?
- O que é a distribuição de Poisson?
- O que as setas no plate notation significam?
- O que os círculos vazios no plate notation significam?
- O que os círculos preenchidos no plate notation significam?
- O que é Latent?
- O que é Dirichlet?
- O que é Allocation?
- O que é estimação de parâmetro?
- O que é inference?
- O que é _variational inference_?
- O que é smoothing?
- Quais são as aplicações do LDA? Explique-as.