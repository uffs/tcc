# Deivid J. Negri
Análise de sentimento. Pelo jeito tenta identificar depressão no Twitter, tanto em português quanto em inglês.
Ele disse que as técnicas utilizadas hoje para indetificar depressão é atráves da análise do que a pessoa expressa. Aí faz sentido usar o Twitter. Deixa de ser "hype".
Falou, na metodologia, sobre o pré-processamento de dados. Eu esqueci de colocar isso nos meus slides.
Falou que vai tirar os dados do twitter com um crawler.
O Marco questionou de onde ele vai tirar os dados que ele vai usar. Ele não soube responder muito bem. Falou que o Denio tem também uma base de dados, mas foi bem vago sobre isso.
O Denio explicou que o que ele está fazendo não é tentar pegar tweet e identificar os deprimidos, mas sim pegar os tweets que tem palavras relacionadas à depressão e classificá-los, se são de propagandas, apoio, etc.

# André Luis Maso
"Internet das coisas aplicado em cidades inteligentes com foco na segurança e integridade de dados".
Usou um slides coloridos bem feios, mas o segundo slide já foi sobre o problema.
Parece estar bem desleixado.
Tinha "hachers" escrito.
O objetivo geral foi bem ruim.

# Guilherme Antunes da Silva
"Análise da tecnologia de conectividade em longa distancia LoRa para IoT"
Não explicou muito o que essa LoRa. Esquece, tá explicando agora.
Uma tecnologia que opera em frequência ISM.
Não entendi muito bem o que ele vai fazer. Tem a ver com distância entre os sinais ou sei lá.
Não entendo muito desses negócios meio físicos.

# Gustavo Henrique Knob
Faltou

# Jardel Anton
"Avaliar os mecanismos de segurança do AWS IoT Device Defender"

# Juliana de Freitas Rosin
Explicou bem.
A melhor até agora.
Entendi sobre o trabalho mesmo sem saber nada sobre milho.
6 objetivos específicos, todos bem feitos.
O slide de trabalhos relacionados estava muito bom. Não só mostrou o que era, mas explicou e expôs o que ela entendeu do artigo. Ela também explicou sobre esse slide. Não esperava que alguém explicasse isso.

# Gabriel Henrique Rudey
"Análise de Chromagramas voltadas à similaridade musical"

# Nicholas Sangoi Brutti
"Proposta de aprendizado de máquina para identificar o meio de transporte baseado em localizações GPS"
Usa dados da série A
Usa processo de Markov
Vai extrair dados de PDF
Vai definir features (Mas em dados estruturados, pelo jeito)

# Ricardo Augusto Müller
"Aplicação de aprendizado de máquina para identificar o meio de transporte baseado em localizações GPS"
Vai, a princípio, usar uma tupla com latitude, longitude e timestamp.
Ainda não tem o dataset, mas tá pra conseguir
Vai usar scikit-learn

# Wagner Osorio Bender
"Utilização de cliente VPN em terminais mascarados por NAT pra acesso remoto de portas TCP/UDP"
Bem objetivo da apresentação
