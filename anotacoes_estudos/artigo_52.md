52 - Probabilistic Topic Models
Survey enviada pelo Denio
David M. Blei

# Question
- Qual o objetivo do survey?
- Esse objetivo é alcançado?
- Quais algoritmos fazem parte do "suite"?
- O que significa analisar a streaming?
- O que é latent?
- O que é dirichlet?
- O que é allocation?
- O que é a  distribuição?
- Como é medida a especificidade de uma palavra dentro de um tópico?
- O que o tamanho das palavras sifnifica na figura 3?
- O que a formula (1) explica? **Rª.**: A _joint distribution_ das variáveis ocultas e visíveis, isso é, a estrutura dos tópicos e as palavras, respectivamente.
- Eu tenho conhecimento suficiente para entender a formula 1? Se não, o que falta?
- O que é computação posterior?
- O que eu preciso saber para entender a notação de "chapas"?
- O que é o topic score?
- A pergunda da página 6 é retórica?
- O que é relaxar os pressupostos do LDA?
- Como incorporar os metadados?
- Qual a direção futura?
- Como usar modelagem de tópicos para descobrir dados?

# Read
- Algoritmos de modelagem de tópicos não precisam de uma categorização prévia. Os tópicos surgem a partir dos textos originais.
- A Intuição por trás do LDA é que um documento possui múltiplos tópicos. Essa distribuição de tópicos sobre o documento facilita situá-lo em uma coleção de documentos.
- Tópicos são formalmente definidos como uma distribuição em um vocabulário fixo.
- A característica definidora do LDA é que ele assume que todos os documentos de uma coleção compartilham o mesmo conjunto de tópicos, mas em uma proporção diferente.
- O que é uma _joint probability distribuition_?
- O que é _conditional/posterior distribution_?
- O problema computacional de inferir a estrutura oculta dos tópicos de um documento é o problema de computar a _distribuição posterior/condicional_ das variáveis ocultas do documento.
- O que são os _topics assignment_, termo usado na descrição formal do LDA? **Rª.**: Cada _z<sub>d,n</sub>_ define o tópico da _n_-ésima palavra do documento _d_. Como as palavras são compartilhadas por todos os tópicos, ter essa distinção é necessária.
- O que é _probabilistic graphical model for LDA_? **Rª.**: Segunda uma anotação de rodapé (e), o campo de modelos gráficos é o que "ilumina" os profundos enlaces entre independência probabilística, teoria dos grafos e algoritmos para computação com distribuições probabilísticas.
- O que é _probabilistic independence_?
- No LDA há algumas dependências, como a atribuição _z<sub>d,n</sub>_ que depende da distribuição _per-document_ _Theta<sub>d</sub>_. Essas dependências definem o LDA e estão codificadas nos pressupostos por traz do processo gerativo, na fórmula matemática particular e na notação de "chapas".
- Uma área ativa da modelagem de tópicos é relaxar os pressupostos do LDA para tentar descobrir estruturas mais sofisticadas nos textos. Um dos modelos dessa área, desenvolvido por "Wallach", pressupõe que as palavras gerados tem um condicional com a palavra anterior. Esse modelo relaxa o pressuposto de _bag of words_. Outro modelo relaxa, da mesma forma, o pressuposto de que a ordem dos documentos não importa. Outro relaxamento é o do pressuposto de que a quantidade de tópicos é conhecida e fixa. Há outras extensões que relaxam outros pressupostos.
- Um dos próximos passos para a modelagem de tópicos é a validação
- Um dos próximos passos é a visualização dos tópicos. "Como podemos explorar essa estrutura da melhor forma?". O autor ainda diz que que essas questões de interface de usuário são essenciais para a modelagem de tópicos.
- Ele diz que uma junção interdisciplinar também é um próximo passo. Cita o caso das áreas de histórica, sociologia, linguística, DIREITO etc que tem o texto como principal fonte de estudo. O LDA pode ajudar a formular hipóteses com base nesses dados.