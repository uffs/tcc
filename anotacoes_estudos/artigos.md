# 1 - A legal citation recommendation engine using topic modeling and semantic similarity
## Útils
- "Aplicar modelagem de tópicos em textos jurídicos ajuda na pesquisa legal"
- "A modelagem de tópicos pode ser utilizada em busca de informações comparando os tópicos de uma busca com os tópicos de um documento"
- Usa LDA para buscar e sugerir documentos enquanto está escrevendo documentos legais. Criam um protótipo para um processador de texto "famoso" para isso.
- Como foi validado o sistema de recomendação?
  **Rªs.**:
  - O corpus é dividido em 80-20 para treinamento e teste, respectivamente;
  - É validada a _perplexity_ dos 20% do corpus a partir do modelo criado pelos 80%;
  - O vetor de distribuição de cada documento é salvo num banco de dados;
    - Buscando as citações relevantes:
    - O _input_, que é o texto digitado pelo usuário, é transformado em um vetor de distribuição com dimensão **K**;
    - A semelhança entre a distribuição calculada e a distribuiçãos de outros documentos é calculada
    - Os documentos são apresentados como recomendações, ordenados pela similaridade semântica, medida por uma métrica de distância de distribuições. O trabalho compara 4 métricas de distâncias: 
      - _Kullback-Leibler divergence_
      - _Information Radius_
      - _Hellinger Distance_
      - _Manhattan Distance_;
    These distance measures were used to create a document-to- document similarity coefficient following  the example of Dagan et al. (1997) and Rus et. al (2013)

    Outra versão do algoritmo testa, além dessas 4 métricas, o peso que os documentos têm de acordo com o número de citações que eles tiveram.
  - Outro teste feito foi o de pegar partes de 1000 parágrafos aleatórios e usá-los como input, depois ver que posição os próprios documentos ficaram (Nos resultados, ele exclui os documentos que foram criados depois do documento que o parágrafo foi retirado foi criado);
> The extensive amount of digitized legal text available for search presents the problem of an ineffective legal search, a problem with which scholars have been trying to cope for several decades [8]
> Indeed, the incorporation of text analysis methods into Law and Artificial Intelligence has been recognized as a pressing need [2]
> The application of collaborative topic modeling to a recommendation system for scientific articles [31] is a utilization of LDA that shares some features with the approach taken in this paper
> Automated content analysis and semantic interpretation of legal text has been implemented for the purposes of case summary [20][21] legal outcome prediction [2]; identification of justices’ ideology [16] or quantifying the "complexity" of a written opinion [22]; determining consensus among justices [23]; attributing authorship in unsigned court opinions [18]; and as a litigation support system in the field of Online Dispute Resolution (ODR) [10].
> The SALOMON system detects relevant legal text by word-frequency techniques and performs retrieval based on similarity measures, while acknowledging the potential of learning topics by the grouping of text [20][21]
> Other models extract semantic information and classify legal text by pre-defined concepts [5], or suggest retrieval based on the k-Nearest Neighbors approach [2][24]. Natural Language Processing techniques have been also implemented in automatic classification of legal case factors [2][32] and in extracting ontologies from legal text [15].
> As has been done in more recent work [10], the tool reported here uses natural language inputs rather than pre-defined, constructed, template-based methods, which were more common in older systems.
> "A vantagem do LDA sobre muitos outros métodos que foram tentados antes é que o LDA assumo que um documento tem uma distribuição sobre vários tópicos, e não que pertence a apenas um tópico, como acontece em algoritmos de clusterização como o K-means.
> Another feature that distinguishes the work reported here is a practical testing technique, which automatically analyzes 1000 queries extracted from a United States Supreme Court Case database.
> The method used in our model for approximating the LDA is the Online Learning algorithm, described by Hoffman et al., a variation on the Expectation Maximization approach [13]
> Since automatic alpha parameter setting was computationally more expensive and yielded no relative benefit, we discontinued it.
_____
# 40 - Text summarization from legal documents: a survey
- Quais são as diferentes formas de sumarização de textos apresentadas pelo artigo?
Não tenho a resposta. No momento que estou e, pela minha análise, essa informação não será importante para mim.
- Quais são as métricas utilizadas para comparar as diferentes formas de sumarização de textos?
> Pelo jeito ele usa uma sumarização já feita por humanos e testa a semelhança entre elas com esse método _ROUGE_ descrito a seguir.
  - **ROUGE** (Recall-Oriented Understudy for Gisting Evaluation): Usa _n-gram co-occurrences statistics_. Possui 5 variações.
    - **ROUGE-N**:
    - **ROUGE-L**: Computa a _Longest common subsequence_ entre as duas sumarizações.
    - **ROUGE-W**: A mesma ideia do _ROUGE-L_, mas com peso nas sequências.
    - **ROUGE-S**: Leva em conta a ocorrência de _skip-bigrams_ nas duas sumarizações. _Skip-bigrams_ são pares de palavras que ocorrem com um intervalo aleatório entre elas.
    - **ROUSE-SU**: É o _ROUGE-S_ com mais coisas.


## útils
- Ele tem uma seção que mostra a diferença entre os textos legais e artigos de jornais ou científicos. Das diferenças, ele cita estrutura, tamanho, vocabulário, ambiguidade e citações.
- "Como sumarização de texto é um problema subjetivo, satisfação humana tem um peso crucial."

_____
# 17 - Finding the Topics of Case Law: Latent Dirichlet Allocation on Supreme Court Decisions
- O que é stemming?
> **Rª.** É remover sufixos e prefixos de palavras para deixá-las na _base_. Por exemplo, as palavras `andaram` e `andarei` possuem o _stem_ `andar`.  
> Pelo que ele tá dizendo, existe um método para Stemming chamado `Snowball`.  
> Uma crítica dele ao processo de _stemming_ é que algumas palavras como _quantitive_ viram palavras não entendíveis (_quant_) pelos experts na hora da validação dos tópicos.
- Ele executa stemming nesse trabalho?
- Com o que ele executa?
- O que é lemmatization?
> **Rª.**
- Ele executa lemmatization nesse trabalho?
> Ele usa 
- Com o que ele executa?
- Qual o critério para ele remover as palavras?
- Como ele cria o vocabulário?
- Como ele remove as palavras?
- Qual a implementação do LDA ele usa?
- Como é feita a validação?
- O que é qualidade de tópico?
- Como saber se um tópico tem qualidade ou não?
- O que é qualidade de distribuição de tópico?
- Como medir isso?
- Quais são os resultados do trabalho?
- Os objetivos foram alcançados?
- Qual o objetivo do trabalho?
- Ele validou com 5 usuários. Como?
- Sobre o que ele falou na conclusão?
- Ele afirma que validação humana sempre deve ser usada. Por que?
- Ele fala sobre conhecimento necessário para validação humana. Explique o que ele quer dizer.
- Quais são as complicações de modelagem de tópico na área jurídica?
- Onde ele usou tópicos intrusos?

> **Tokenização** é quando transforma-se um texto em um saco de palavras, retirando pontuação, etc.  
> Ele usou o `Treebank tokenizer` do `NLTK` para tokenizar os textos.
_____
# 16 - An Analysis of Topic Modelling for Legislative Texts
_____
# 57 - Agente para Extração de Informações de Notas de Expediente de Processos Jurídicos
- Partes úteis do texto para citação
> São atos do processo: a petição inicial (documento elaborado pelo autor que dá inicio ao processo), citação (forma de dar ciência ao réu sobre a instauração do processo e oportunizar a sua defesa), contestação (defesa do réu), réplica (manifestação do autor sobre a contestação), intimação das partes para produção de provas (o juiz pede a comprovação das alegações) e sentença (decisão do juiz sobre o litígio)  
> **Nota** Usar isso para falar sobre a estrutura das publicações

> As intimações têm como objetivo informar um ato ou termo
processual, bem como convocar a parte a fazer ou abster-se de fazer alguma coisa.
> São de grande relevância, porque é a partir delas que começam a fluir os prazos para que as partes  exerçam os direitos e faculdades processuais  
> **Nota** Bom para colocar na problematização. Falar sobre como é importante estar bem classificada a base. Detalhe: É uma citação a outro artigo, portanto tem que usar aquela referência especial.

> A preclusão de um ato como a contestação, por exemplo, tem como conseqüência a aceitação dos fatos alegados pelo autor como verdadeiros.
> Por esta razão, a observância dos prazos é de fundamental importância para
um advogado.  
> **Nota** Mesma coisa que a nota acima, com a excessão de que esse não é citação de outro artigo.

> As notas de expediente são publicadas pelos cartórios nos Diários Oficiais de Justiça.
> Apesar das notas conterem texto livre, há uma certa padronização na sua escrita.
> Dados como a data de publicação, o número do processo, a vara ou a câmara em que o processo está tramitando, a matéria, o número da nota, o tipo de recurso e as partes envolvidas no processo em geral estão  sempre presentes, como pode ser observado na Figura 3  
> **Nota** Pra falar sobre a estrutura das publicações